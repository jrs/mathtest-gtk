PKGCONFIG32=PKG_CONFIG_PATH=/usr/i686-w64-mingw32/lib/pkgconfig
PKGCONFIG64=PKG_CONFIG_PATH=/usr/x86_64-w64-mingw32/lib/pkgconfig

CFLAGS=-O3 -Wall -lm `pkg-config --cflags --libs gtk+-3.0` -I.

all:
	gcc math-gtk.c calculate.c -o math-gtk $(CFLAGS)

win32:
	i686-w64-mingw32-gcc math-gtk.c calculate.c -o math-gtk.exe $(CFLAGS)

win64:
	x86_64-w64-mingw32-gcc math-gtk.c calculate.c -o math-gtk.exe $(CFLAGS)

clean:
	rm math-gtk
