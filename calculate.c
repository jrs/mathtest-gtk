/*
 * Random math test
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <math.h>
#include "math-gtk.h"

double operator () {
	double product;
	switch (o) {
	case 0:
		return l + r;
	case 1:
		return l - r;
	case 2:
		return l * r;
	case 3:
		product = round (l / r * 100);
		return product / 100;
	}
	return 0;
}

int checkAnswer (GtkWidget *widget) {
	double in;
	const char *line = gtk_entry_get_text(GTK_ENTRY (widget));
	
	if (sscanf(line, "%lf", &in) && strlen(line) > 0) {
		if (ans == in) {
			
			return 1;
		}
		else {
			return 0;
		}
	}
	return 2;
		
}

//initialise variables and update text, don't test anything though
int startQuestion (MyWidgets *widgets) {

	//generate question parameters
	l = rand() % 20;
	r = rand() % 20;
	o = rand() % 4;
	ans = operator(widgets);

	char stringbuff[64];
	snprintf(stringbuff, sizeof(stringbuff), "What is the answer to %.02g %c %.02g?", l, o == 0 ? '+' : o == 1 ? '-' : o == 2 ? 'x' : '/', r);

	gtk_label_set_text (GTK_LABEL(widgets->label), stringbuff);
	gtk_button_set_label (GTK_BUTTON(widgets->button), "Submit");

	return 0;
}
