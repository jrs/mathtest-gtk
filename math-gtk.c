#include <gtk/gtk.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "math-gtk.h"
#include "calculate.h"


int corr = 0;
int total = 0;
int state = -1;
double l, r, ans;
int o;

void stats(MyWidgets *widgets) {

	
	gtk_widget_show_all (widgets->statswindow);
}

void clearbuff(MyWidgets *widgets) {
	
	gtk_widget_grab_focus(widgets->entry); 
	GtkEntryBuffer *buff = gtk_entry_buffer_new (NULL, -1);
	gtk_entry_set_buffer(GTK_ENTRY(widgets->entry), buff);		
}

//program progression monitored via int state
static void logic (MyWidgets *widgets) {
	char stringbuff[64];
	switch (state) {
	case -1:
		startQuestion(widgets);
		gtk_widget_set_sensitive (widgets->entry, TRUE);
		gtk_widget_grab_focus(widgets->entry); 
		total += 1;
		gtk_statusbar_push(GTK_STATUSBAR(widgets->statbar), -2, "If required, use 2 significant figures.");
		state = 0;
		return;
	case 0:
		clearbuff(widgets);
		snprintf(stringbuff, sizeof(stringbuff), "Incorrect! (The correct answer was %.*f)", o == 3 ? 2 : 0,(float)ans); 
		startQuestion(widgets);
		total += 1;
		gtk_statusbar_push(GTK_STATUSBAR(widgets->statbar), 0, stringbuff);
		break;
	case 1:
		clearbuff(widgets);
		startQuestion(widgets);
		total += 1;
		gtk_statusbar_push(GTK_STATUSBAR(widgets->statbar), 1, "Correct!");
		break;
	case 2:
		clearbuff(widgets);
		gtk_statusbar_push(GTK_STATUSBAR(widgets->statbar), 2, "Invalid input, try again.");
				
	}
}
		
static void enterpress (struct _Widgets *widgets) {
	state = checkAnswer(widgets->entry);
	logic(widgets);
}


static void buttonpress (struct _Widgets *widgets) {
	if (state == -1){
		logic(widgets);
	}
	else {
	state = checkAnswer(widgets->entry);
	logic(widgets);
	}
}
static void activate (GtkApplication* app, gpointer user_data)
{
	GtkWidget *window;
	GtkWidget *box;
	GtkWidget *statsbutton;
	MyWidgets *widgets = g_new(MyWidgets, 1);
	window = gtk_application_window_new (app);
	gtk_window_set_title (GTK_WINDOW (window), "Mathtest-GTK");
	gtk_window_set_default_size (GTK_WINDOW (window), 370, 160);

	widgets->statswindow = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	g_signal_connect (widgets->statswindow, "delete_event", G_CALLBACK (gtk_widget_hide), NULL);	
	gtk_container_set_border_width (GTK_CONTAINER (window), 10);
	box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_add (GTK_CONTAINER (window), box);
	
	gtk_widget_show (box);
 
	widgets->label = gtk_label_new ("Welcome to Mathtest-GTK!");
	gtk_box_pack_start (GTK_BOX (box), widgets->label, FALSE, FALSE, 0);

	widgets->entry = gtk_entry_new();
	gtk_entry_set_max_length (GTK_ENTRY(widgets->entry), 64);
	g_signal_connect_swapped (widgets->entry, "activate", G_CALLBACK (enterpress), widgets);
	gtk_box_pack_start (GTK_BOX (box), widgets->entry, TRUE, TRUE, 5);
	
	widgets->button = gtk_button_new_with_label ("Begin!");
	g_signal_connect_swapped (widgets->button, "clicked", G_CALLBACK (buttonpress), widgets);  	
	gtk_box_pack_start (GTK_BOX (box), widgets->button, FALSE, FALSE, 5);
	
	statsbutton = gtk_button_new_with_label ("View stats");
	g_signal_connect_swapped (statsbutton, "clicked", G_CALLBACK (stats), widgets);
	gtk_box_pack_start (GTK_BOX (box), statsbutton, FALSE, FALSE, 0);

	widgets->statbar = gtk_statusbar_new ();
	gtk_statusbar_push(GTK_STATUSBAR(widgets->statbar), -1, "Please click the button to begin.");
	gtk_box_pack_end (GTK_BOX (box), widgets->statbar, FALSE,FALSE,0);

	gtk_widget_set_sensitive (widgets->entry, FALSE);
	
	gtk_widget_show_all (window);

}

int main (int argc, char **argv) {
	GtkApplication *app;
	int status;
	srand(time(NULL));

	app = gtk_application_new ("com.coptinet.mathtest-gtk", G_APPLICATION_FLAGS_NONE);
	g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
	status = g_application_run (G_APPLICATION (app), argc, argv);
	g_object_unref (app);

	return status;
}

